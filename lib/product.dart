import 'package:firstapp/viewdetail.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Product {
  final String prod_name;
  final int prod_price;
  final String prod_img;
  final int prod_id;

  Product({
    required this.prod_name,
    required this.prod_price,
    required this.prod_img,
    required this.prod_id,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      prod_name: json['prod_name'],
      prod_price: int.parse(json['prod_price']),
      prod_img: json['prod_img'],
      prod_id: int.parse(json['prod_id']),
    );
  }
}

class ProductListScreen extends StatefulWidget {
  int cat_id = 0;

  ProductListScreen({required cat_id}) {
    print(cat_id);
    this.cat_id = cat_id;
  }

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  late int prod_id = 0;

  List<Product> products = [];

  bool _isGridView = false;

  void _toggleView() {
    setState(() {
      _isGridView = !_isGridView;
    });
  }

  @override
  void initState() {
    super.initState();
    fetchProducts();
  }

  Future<void> fetchProducts() async {
    final response = await http.get(
      Uri.parse(
          'http://192.168.251.119/flutterapi/getproduct.php?cat_id=${widget.cat_id}'),
    );

    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {
      final jsonList = json.decode(response.body) as List<dynamic>;
      setState(() {
        products = jsonList.map((json) => Product.fromJson(json)).toList();
      });
    } else {
      throw Exception('Failed to load data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Products'),
      //   actions: [
      //     IconButton(
      //       onPressed: _toggleView,
      //       icon: Icon(_isGridView ? Icons.list : Icons.grid_view),
      //     ),
      //   ],
      // ),
      body: _isGridView
          ? GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                // crossAxisSpacing: 10,
                crossAxisCount: 2,
              ),
              itemCount: products.length,
              itemBuilder: (context, index) {
                final product = products[index];
                return Column(
                  children: [
                    ListTile(
                      leading: Image.network(
                          'http://192.168.251.119/flutterapi/images/' +
                              product.prod_img),
                      title: Text(product.prod_name),
                      subtitle: Text('\$${product.prod_price}'),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ViewProductScreen(
                              prod_id: product.prod_id,
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                );
              },
            )
          : ListView.builder(
              itemCount: products.length,
              itemBuilder: (context, index) {
                final product = products[index];
                return Container(
                  child: Column(
                    children: [
                      ListTile(
                        leading: Image.network(
                            'http://192.168.251.119/flutterapi/images/' +
                                product.prod_img),
                        title: Text(product.prod_name),
                        subtitle: Text('\$${product.prod_price}'),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ViewProductScreen(
                                prod_id: product.prod_id,
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: _toggleView,
        child: Icon(_isGridView ? Icons.list : Icons.grid_view),
      ),
    );
  }
}
