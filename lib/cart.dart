import 'package:firstapp/placeorder.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class Cart {
  final int cart_id;
  final int prod_id;
  final String prod_name;
  final int prod_price;
  final int prod_qty;
  final String prod_img;
  final int user_id;

  Cart({
    required this.cart_id,
    required this.prod_id,
    required this.prod_name,
    required this.prod_price,
    required this.prod_qty,
    required this.prod_img,
    required this.user_id,
  });

  factory Cart.fromJson(Map<String, dynamic> json) {
    return Cart(
      cart_id: int.parse(json['cart_id']),
      prod_id: int.parse(json['prod_id']),
      prod_name: json['prod_name'],
      prod_price: int.parse(json['prod_price']),
      prod_qty: int.parse(json['prod_qty']),
      prod_img: json['prod_img'],
      user_id: int.parse(json['user_id']),
    );
  }
}

class CartScreen extends StatefulWidget {
  final int prod_id;
  final String prod_name;
  final int prod_price;
  final String prod_img;


  CartScreen({
    required this.prod_id,
    required this.prod_name,
    required this.prod_price,
    required this.prod_img,
  });

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  List<Cart> cartItems = [];
  double totalCartValue = 0;

  @override
  void initState() {
    super.initState();
    print(widget.prod_id);
    _getCartItems();
  }

  void _getCartItems() async {
    var prefs = await SharedPreferences.getInstance();
    var url =
        Uri.http("192.168.251.119", '/flutterapi/addtocart.php', {'q': '{http}'});
    var response = await http.post(url, body: {
      "prod_id": widget.prod_id.toString(),
      "prod_name": widget.prod_name,
      "prod_price": widget.prod_price.toString(),
      "prod_img": widget.prod_img,
      "user_id": prefs.getInt("userid").toString()
    });

    print(response.statusCode);
    print(response.body);

    final responseData = json.decode(response.body);
    setState(() async {
      var url =
          Uri.http("192.168.251.119", '/flutterapi/viewcart.php', {'q': '{http}'});
      var response = await http
          .post(url, body: {"user_id": prefs.getInt("userid").toString()});

      print(response.statusCode);
      print(response.body);

      if (response.statusCode == 200) {
        final jsonList = json.decode(response.body) as List<dynamic>;
        setState(() {
          print(cartItems);
          cartItems = jsonList.map((json) => Cart.fromJson(json)).toList();
          print(cartItems[0].prod_name);

          for (Cart c in cartItems) {
            totalCartValue = totalCartValue + c.prod_price;
          }
        });
      } else {
        throw Exception('Failed to load data');
      }
    });
  }

  void _removeCartItem(int cartItemId) async {
    print(cartItemId);
    var url =
        Uri.http("192.168.251.119", '/flutterapi/deletecart.php', {'q': '{http}'});
    var response = await http.post(url, body: {
      "cart_id": cartItemId.toString(),
    });

    final responseData = json.decode(response.body);
    print(responseData);
    if (responseData['success'] == true) {
      setState(() {
        cartItems.removeWhere((item) => item.cart_id == cartItemId);
        totalCartValue = 0;
        for (Cart c in cartItems) {
          totalCartValue = totalCartValue + c.prod_price;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: cartItems.length,
              itemBuilder: (context, index) {
                final item = cartItems[index];
                return Card(
                  child: ListTile(
                    leading: Image.network(
                      'http://192.168.251.119/flutterapi/images/' + item.prod_img,
                      width: 50,
                    ),
                    title: Text(item.prod_name),
                    subtitle: Text(
                      '\$${item.prod_price} ',
                    ),
                    trailing: IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: (){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('Confirm'),
                              content: const Text('Are you sure you want to delete this item?'),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('Cancel'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                TextButton(
                                  child: const Text('Delete'),
                                  onPressed: () {
                                    _removeCartItem(item.cart_id);
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                    ),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Total: \$${totalCartValue.toStringAsFixed(2)}',
              style: TextStyle(fontSize: 20),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              var index = 0;
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PlaceOrderPage(
                    prod_id: cartItems[index].prod_id,
                    prod_name: cartItems[index].prod_name,
                    prod_price: cartItems[index].prod_price,
                    prod_qty: cartItems[index].prod_qty,
                    inv_amt: totalCartValue,
                  ),
                ),
              );
            },
            child: Text("Checkout"),
          ),
        ],
      ),
    );
  }
}
