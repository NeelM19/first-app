import 'dart:convert';
import 'package:firstapp/cart.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Product {
  final int prod_id;
  final int cat_id;
  final String prod_name;
  final String prod_desc;
  final int prod_price;
  final String prod_img;
  final String status;

  Product({
    required this.prod_id,
    required this.cat_id,
    required this.prod_name,
    required this.prod_desc,
    required this.prod_price,
    required this.prod_img,
    required this.status,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      prod_id: int.parse(json['prod_id']),
      cat_id: int.parse(json['cat_id']),
      prod_name: json['prod_name'],
      prod_desc: json['prod_desc'],
      prod_price: int.parse(json['prod_price']),
      prod_img: json['prod_img'],
      status: json['status'],
    );
  }
}

class ViewProductScreen extends StatefulWidget {
  late int prod_id = 0;

  ViewProductScreen({required prod_id}) {
    print(prod_id);
    this.prod_id = prod_id;
  }

  @override
  _ViewProductScreen createState() => _ViewProductScreen();
}

class _ViewProductScreen extends State<ViewProductScreen> {
  late Future<Product> _futureProduct;

  bool passToggle = true;
  bool checkValue = false;

  late SharedPreferences prefs;

  TextEditingController user_id = TextEditingController();

  @override
  void initState() {
    super.initState();
    _futureProduct = fetchProductDetails(widget.prod_id);
  }

  Future<Product> fetchProductDetails(int prod_id) async {
    prefs = await SharedPreferences.getInstance();
    var url = Uri.http(
        "192.168.251.119", '/flutterapi/viewproduct.php', {'q': '{http}'});

    var response = await http.post(url, body: {
      "prod_id": prod_id.toString(),
    });
    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {
      getCredential();
      print("i m here");
      print(prefs.getInt("userid"));

      print(json.decode(response.body));
      final jsonresponse = json.decode(response.body);

      return Product.fromJson(jsonresponse[0]);
      // return 0;
    } else {
      throw Exception('Failed to load product details');
    }
  }

  getCredential() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      checkValue = prefs.getBool("check")!;
      if (checkValue != null) {
        if (checkValue) {

        } else {
          user_id.clear();
          prefs.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Product Detail'),
      // ),
      body: Center(
        child: FutureBuilder<Product>(
          future: _futureProduct,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 16),
                  Image.network(
                    'http://192.168.251.119/flutterapi/images/' +
                        snapshot.data!.prod_img,
                    height: 180,
                    width: 180,
                  ),
                  const SizedBox(height: 16),
                  Text(snapshot.data!.prod_name,
                      style: const TextStyle(fontSize: 24)),
                  const SizedBox(height: 16),
                  Text(snapshot.data!.prod_desc,
                      style: const TextStyle(fontSize: 18)),
                  const SizedBox(height: 16),
                  Text('\$${snapshot.data!.prod_price}',
                      style: const TextStyle(fontSize: 24)),
                  Container(
                    padding: const EdgeInsets.all(123),
                    child: ElevatedButton(
                      onPressed: () {
                        ScaffoldMessenger.maybeOf(context)?.showSnackBar(
                          const SnackBar(
                            backgroundColor: Colors.green,
                            content: Text('Item added to a cart!!'),
                          ),
                        );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => CartScreen(
                              prod_id: snapshot.data!.prod_id,
                              prod_name: snapshot.data!.prod_name,
                              prod_price: snapshot.data!.prod_price,
                              prod_img: snapshot.data!.prod_img,
                              // user_id: '',
                            ),
                          ),
                        );
                      },
                      child: Text('Add To Cart'),
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }

            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
