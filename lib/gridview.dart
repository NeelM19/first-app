import 'package:firstapp/login.dart';
import 'package:firstapp/register.dart';
import 'package:flutter/material.dart';

class Grid extends StatefulWidget {
  @override
  Gridview createState() {
    return new Gridview();
  }
}

class Gridview extends State<Grid> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // drawer: Drawer(
        //   child: ListView(
        //     padding: EdgeInsets.zero,
        //     children: [
        //       const UserAccountsDrawerHeader(
        //         otherAccountsPictures: [
        //           CircleAvatar(
        //             backgroundImage: NetworkImage(
        //                 'https://img.icons8.com/pastel-glyph/2x/user-male.png'),
        //             backgroundColor: Colors.white,
        //           ),
        //         ],
        //         accountName: Text('Neel Modi'),
        //         accountEmail: Text('neelmodi@gmail.com'),
        //         currentAccountPicture: CircleAvatar(
        //           backgroundImage: AssetImage('images/nature.jpg'),
        //         ),
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.login),
        //         title: const Text('Login'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyLogin(),
        //             ),
        //           );
        //         },
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.app_registration),
        //         title: const Text('Register'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyRegister(),
        //             ),
        //           );
        //         },
        //       ),
        //       const AboutListTile(
        //         applicationName: 'Apple',
        //         applicationIcon: Icon(Icons.apple),
        //         icon: Icon(Icons.safety_check),
        //       ),
        //     ],
        //   ),
        // ),
        // appBar: AppBar(
        //   title: const Text("GridView"),
        // ),456
        body: Center(
          child: GridView.extent(
            primary: false,
            padding: const EdgeInsets.all(16),
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
            maxCrossAxisExtent: 200.0,
            children: <Widget>[
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('images/login.png'), fit: BoxFit.cover),
                ),
                padding: const EdgeInsets.all(8),
                child: const Text('First', style: TextStyle(fontSize: 20)),
                //color: Colors.yellow,
              ),
              Container(
                padding: const EdgeInsets.all(8),
                child: const Text('Second', style: TextStyle(fontSize: 20)),
                color: Colors.blue,
              ),
              Container(
                padding: const EdgeInsets.all(8),
                child: Image.asset('images/register.png'),
                // Text('Third', style: TextStyle(fontSize: 20)),
                color: Colors.blue,
              ),
              Container(
                padding: const EdgeInsets.all(8),
                child: Text('Four', style: TextStyle(fontSize: 20)),
                color: Colors.yellow,
              ),

              // Applying List View

              ListView(
                padding: const EdgeInsets.all(8),
                children: <Widget>[
                  Container(
                    height: 50,
                    color: Colors.green,
                    child: const Center(child: Text('List view 1')),
                  ),
                  Container(
                    height: 50,
                    color: Colors.red,
                    child: const Center(child: Text('List view 2')),
                  ),
                  Container(
                    height: 50,
                    color: Colors.blue,
                    child: const Center(child: Text('List view 3')),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
