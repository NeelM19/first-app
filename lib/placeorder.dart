import 'dart:convert';
import 'package:firstapp/categories.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class PlacedOrder {
  final int od_id;
  final int order_id;
  final int prod_id;
  final String prod_name;
  final String prod_img;
  final int prod_price;
  final int prod_qty;
  final int inv_amt;

  PlacedOrder({
    required this.od_id,
    required this.order_id,
    required this.prod_id,
    required this.prod_name,
    required this.prod_img,
    required this.prod_price,
    required this.prod_qty,
    required this.inv_amt,
  });

  factory PlacedOrder.fromJson(Map<String, dynamic> json) {
    return PlacedOrder(
      od_id: int.parse(json['od_id']),
      order_id: int.parse(json['order_id']),
      prod_id: int.parse(json['prod_id']),
      prod_name: json['prod_name'],
      prod_img: json['prod_img'],
      prod_price: int.parse(json['prod_price']),
      prod_qty: int.parse(json['prod_qty']),
      inv_amt: int.parse(json['inv_amt']),
    );
  }
}

class PlaceOrderPage extends StatefulWidget {
  late int prod_id;
  late String prod_name;
  late int prod_price;
  late int prod_qty;
  late double inv_amt;

  // constructor and other code

  PlaceOrderPage({
    required this.prod_id,
    required this.prod_name,
    required this.prod_price,
    required this.prod_qty,
    required this.inv_amt,
  });

  @override
  _PlaceOrderPageState createState() => _PlaceOrderPageState();
}

class _PlaceOrderPageState extends State<PlaceOrderPage> {
  double totalOrderPlaced = 0;
  List<PlacedOrder> cartItems = [];
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    print(widget.prod_id);
    _getCartItems();
  }

  void _getCartItems() async {
    var prefs = await SharedPreferences.getInstance();
    var url = Uri.http(
        "192.168.251.119", '/flutterapi/order_confirm.php', {'q': '{http}'});

    var response = await http.post(url, body: {
      "user_id": prefs.getInt("userid").toString(),
      "inv_amt": widget.inv_amt.toString(),
    });

    print(response.statusCode);
    print(response.body);

    final responseData = json.decode(response.body);
    setState(() async {
      var url = Uri.http(
          "192.168.251.119", '/flutterapi/vieworder.php', {'q': '{http}'});
      var response = await http
          .post(url, body: {"user_id": prefs.getInt("userid").toString()});

      print(response.statusCode);
      print(response.body);

      if (response.statusCode == 200) {
        final jsonList = json.decode(response.body) as List<dynamic>;
        setState(() {
          cartItems =
              jsonList.map((json) => PlacedOrder.fromJson(json)).toList();
          // print(cartItems[0]['prod_name']);

          for (PlacedOrder c in cartItems) {
            totalOrderPlaced = totalOrderPlaced + c.inv_amt;
          }
        });
      } else {
        throw Exception('Failed to load data');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Place Order'),
      // ),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : _buildOrderForm(),
    );
  }

  Widget _buildOrderForm() {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: cartItems.length,
              itemBuilder: (context, index) {
                final item = cartItems[index];
                return Card(
                  child: ListTile(
                    leading: Image.network(
                      'http://192.168.251.119/flutterapi/images/' +
                          item.prod_img,
                      width: 50,
                    ),
                    title: Text(item.prod_name),
                    subtitle: Text(
                      '\$${item.prod_price} ',
                    ),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Total: \$${totalOrderPlaced}',
              style: const TextStyle(fontSize: 20),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CategoriesPage()));
            },
            child: const Text('Confirm Your Order'),
          ),
        ],
      ),
    );
  }
}
