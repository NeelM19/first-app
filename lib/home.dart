import 'package:firstapp/bottom_navigation_bar.dart';
import 'package:firstapp/carousel_slider.dart';
import 'package:firstapp/gridview.dart';
import 'package:firstapp/hobbies.dart';
import 'package:firstapp/login.dart';
import 'package:firstapp/radio_button.dart';
import 'package:firstapp/register.dart';
import 'package:firstapp/tabbar.dart';
import 'package:flutter/material.dart';

class MyHome extends StatefulWidget {
  @override
  _MyHome createState() {
    return new _MyHome();
  }
}

class _MyHome extends State<MyHome> {
  String buttonName = 'Click';
  int currentIndex = 0;

  bool valuefirst = false;
  bool valuesecond = false;
  bool valuethird = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // backgroundColor: Colors.white,
        // drawer: Drawer(
        //   child: ListView(
        //     padding: EdgeInsets.zero,
        //     children: [
        //       const UserAccountsDrawerHeader(
        //         otherAccountsPictures: [
        //           CircleAvatar(
        //             backgroundImage: NetworkImage(
        //                 'https://img.icons8.com/pastel-glyph/2x/user-male.png'),
        //             backgroundColor: Colors.white,
        //           ),
        //         ],
        //         accountName: Text('Neel Modi'),
        //         accountEmail: Text('neelmodi@gmail.com'),
        //         currentAccountPicture: CircleAvatar(
        //           backgroundImage: AssetImage('images/nature.jpg'),
        //         ),
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.login),
        //         title: const Text('Login'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyLogin(),
        //             ),
        //           );
        //         },
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.app_registration),
        //         title: const Text('Register'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyRegister(),
        //             ),
        //           );
        //         },
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.category),
        //         title: const Text('Categories'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => CategoriesPage(),
        //             ),
        //           );
        //         },
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.api),
        //         title: const Text('Pagination'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => PostsOverviewScreen(),
        //             ),
        //           );
        //         },
        //       ),
        //       const AboutListTile(
        //         applicationName: 'Apple',
        //         applicationIcon: Icon(Icons.apple),
        //         icon: Icon(Icons.safety_check),
        //       ),
        //     ],
        //   ),
        // ),
        // appBar: AppBar(
        //   title: Text('Hobbies'),
        //   actions: [
        //     IconButton(
        //       onPressed: showBottomSheet,
        //       icon: Icon(Icons.open_in_new),
        //     )
        //   ],
        // ),
        body: Column(
          children: <Widget>[
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.trailing,
              title: const Text('Football'),
              value: this.valuefirst,
              onChanged: (value) {
                setState(() {
                  this.valuefirst = value!;
                });
              },
            ),
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.trailing,
              title: const Text('Cricket'),
              value: this.valuesecond,
              onChanged: (value) {
                setState(() {
                  this.valuesecond = value!;
                });
              },
            ),
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.trailing,
              title: const Text('Basketball'),
              value: this.valuethird,
              onChanged: (value) {
                setState(() {
                  this.valuethird = value!;
                });
              },
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHobbies(),
                      ),
                  );
              },
              child: const Text('Next Page'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BottomNav(),
                  ),
                );
              },
              child: const Text('Bottom Navigation'),
            ),
            ElevatedButton(
              onPressed: showBottomSheet,
              child: const Text('BottomSheet'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MySlider(),
                  ),
                );
              },
              child: const Text('Caraousel Slider'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Tabbarr(),
                  ),
                );
              },
              child: const Text('Tab Bar'),
            ),
          ],
        ),

        /* bottom navigation */

        // bottomNavigationBar: CurvedNavigationBar(
        //   buttonBackgroundColor: Colors.black,
        //   animationCurve: Curves.easeInBack,
        //   backgroundColor: Colors.deepPurple,
        //   color: Colors.deepPurple.shade200,
        //   animationDuration: const Duration(milliseconds: 300),
        //   onTap: (index) {
        //     print(index);
        //   },
        //   items: const [
        //     Icon(
        //       Icons.home,
        //       color: Colors.white,
        //     ),
        //     Icon(
        //       Icons.favorite,
        //       color: Colors.white,
        //     ),
        //     Icon(
        //       Icons.settings,
        //       color: Colors.white,
        //     ),
        //   ],
        // ),
      ),
    );
  }

  void showBottomSheet() => showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        // barrierColor: Colors.orange.withOpacity(0.2),
        context: context,
        builder: (context) => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: Icon(Icons.app_registration),
              title: Text('Register'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyRegister(),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.login),
              title: Text('Login'),
              onTap: () {
                Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MyLogin(),
                ),
              );
              },
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHome(),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.games),
              title: Text('Hobbies'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHobbies(),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.radio_button_checked),
              title: Text('Radio'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyRadio(),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.grid_view),
              title: Text('Grid'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Grid(),
                  ),
                );
              },
            ),
          ],
        ),
      );
}
