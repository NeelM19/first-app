import 'dart:async';
import 'package:firstapp/bottom_navigation_bar.dart';
import 'package:firstapp/categories.dart';
import 'package:firstapp/home.dart';
import 'package:firstapp/intro_screen.dart';
import 'package:firstapp/login.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(const SplashScreen());
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    Timer(
      const Duration(seconds: 5),
      () async {
        prefs = await SharedPreferences.getInstance();
        var check = prefs.getBool("check");
        // print(check);
        if (check == true) {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => BottomNav(),
            ),
          );
        } else {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => const IntroScreen(),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Lottie.network(
            'https://assets10.lottiefiles.com/packages/lf20_jmejybvu.json',
          ),
        ),
      ),
    );
  }
}
