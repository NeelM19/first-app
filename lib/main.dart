import 'package:firebase_core/firebase_core.dart';
import 'package:firstapp/bottom_navigation_bar.dart';
import 'package:firstapp/categories.dart';
import 'package:firstapp/hobbies.dart';
import 'package:firstapp/login.dart';
import 'package:firstapp/post_list.dart';
import 'package:firstapp/radio_button.dart';
import 'package:firstapp/register.dart';
import 'package:firstapp/splash_screen.dart';
import 'package:firstapp/tabbar.dart';
import 'package:flutter/material.dart';
import 'package:firstapp/home.dart';
import 'package:firstapp/gridview.dart';
import 'package:firstapp/carousel_slider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const SplashScreen(),
      routes: {
        'register': (context) => const MyRegister(),
        'login': (context) => const MyLogin(),
        'home': (context) => MyHome(),
        'hobbies': (context) => MyHobbies(),
        'radiopage': (context) => MyRadio(),
        'gridview': (context) => Grid(),
        'bottomnav': (context) => BottomNav(),
        'slider': (context) => MySlider(),
        'tab': (context) => const Tabbarr(),
        'page': (context) => PostsOverviewScreen(),
        'categories': (context) => CategoriesPage(),
      },
    ),
  );
}
