import 'package:firstapp/cart.dart';
import 'package:firstapp/change_password.dart';
import 'package:firstapp/home.dart';
import 'package:firstapp/product.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class ViewCartProduct {
  final int prod_id;
  final int cat_id;
  final String prod_name;
  final String prod_desc;
  final int prod_price;
  final String prod_img;
  final String status;

  ViewCartProduct({
    required this.prod_id,
    required this.cat_id,
    required this.prod_name,
    required this.prod_desc,
    required this.prod_price,
    required this.prod_img,
    required this.status,
  });

  factory ViewCartProduct.fromJson(Map<String, dynamic> json) {
    return ViewCartProduct(
      prod_id: int.parse(json['prod_id']),
      cat_id: int.parse(json['cat_id']),
      prod_name: json['prod_name'],
      prod_desc: json['prod_desc'],
      prod_price: int.parse(json['prod_price']),
      prod_img: json['prod_img'],
      status: json['status'],
    );
  }
}

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  late Future<ViewCartProduct> _futureProduct;
  late int cat_id = 0;
  List _data = [];
  List<Cart> cartItems = [];
  double totalCartValue = 0;

  @override
  void initState() {
    super.initState();
    _fetchData();
    _futureProduct = _getCartItems();
  }

  void _fetchData() async {
    final response = await http
        .get(Uri.parse('http://192.168.251.119/flutterapi/getcategory.php'));
    if (response.statusCode == 200) {
      setState(() {
        _data = json.decode(response.body);
      });
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<ViewCartProduct> _getCartItems() async {
    var prefs = await SharedPreferences.getInstance();
    var url = Uri.http(
        "192.168.251.119", '/flutterapi/viewcart.php', {'q': '{http}'});
    var response = await http.post(
      url,
      body: {"user_id": prefs.getInt("userid").toString()},
    );

    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {
      final jsonList = json.decode(response.body);
      return ViewCartProduct.fromJson(jsonList[0]);
    } else {
      throw Exception('Failed to load data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Categories'),
      //   actions: [
      //     IconButton(
      //       onPressed: () {
      //         // var index ;
      //         // Navigator.push(
      //         //   context,
      //           // MaterialPageRoute(
      //             // future: _futureProduct,
      //             // builder: (context) => CartScreen(prod_id: 0, prod_name: '', prod_price: 0, prod_img: '',
      //               // prod_id: null,
      //               // prod_name: _data[index]['prod_name'],
      //               // prod_price: _data[index]['prod_price'],
      //               // prod_img: _data[index]['prod_img'],
      //               // user_id: '',
      //             // ),
      //         //   ),
      //         // );
      //       },
      //       icon: Icon(Icons.shopping_cart),
      //     ),
      //   ],
      // ),
      // drawer: Drawer(
      //   child: ListView(
      //     padding: EdgeInsets.zero,
      //     children: [
      //       const SizedBox(height: 90),
      //       ListTile(
      //         leading: const Icon(Icons.password),
      //         title: const Text('Change Password'),
      //         onTap: () {
      //           Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePassword()));
      //         },
      //       ),
      //     ],
      //   ),
      // ),
      body: GridView.builder(
        itemCount: _data.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10.0,
          crossAxisSpacing: 10.0,
          childAspectRatio: 1.0,
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  leading: Image.network(
                    'http://192.168.251.119/flutterapi/images/' +
                        _data[index]['img'],
                    height: 400,
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProductListScreen(
                          cat_id: int.parse(
                            _data[index]['cat_id'],
                          ),
                        ),
                      ),
                    );
                  },
                ),

                // Text('${_data[index]['cat_id']}'),

                TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProductListScreen(
                          cat_id: int.parse(
                            _data[index]['cat_id'],
                          ),
                        ),
                      ),
                    );
                  },
                  child: Text('${_data[index]['cat_name']}'),
                ),

                Text('${_data[index]['cat_desc']}'),
              ],
            ),
          );
        },
      ),
    );
  }
}
