import 'package:firstapp/bottom_navigation_bar.dart';
import 'package:firstapp/login.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';


class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: IntroductionScreen(
          globalBackgroundColor: Colors.white,
          scrollPhysics: const BouncingScrollPhysics(),
          pages: [
            PageViewModel(
              titleWidget: const Text(
                "Welcome",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              body: 'Welcome to our App. Lets shop!',
              image: Image.asset(
                'images/friendship.png',
                height: 300,
                width: 300,
              ),
            ),
            PageViewModel(
              titleWidget: const Text(
                "Welcome",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              body: 'We help people connect \n with store all around India',
              image: Image.asset(
                'images/friendship.png',
                height: 300,
                width: 300,
              ),
            ),
            PageViewModel(
              titleWidget: const Text(
                "Welcome",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              body: 'We show the easy way to shop\njust stay at home with us',
              image: Image.asset(
                'images/friendship.png',
                height: 300,
                width: 300,
              ),
            ),
          ],
          onDone: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => const MyLogin()));
          },
          onSkip: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => BottomNav()));
          },
          showSkipButton: true,
          skip: const Text(
            'Skip',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
              color: Colors.blueAccent,
            ),
          ),
          next: const Icon(
            Icons.arrow_forward,
            color: Colors.blueAccent,
          ),
          done: const Text(
            'Done',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
              color: Colors.blueAccent,
            ),
          ),
          dotsDecorator: DotsDecorator(
              size: const Size.square(10),
              activeSize: const Size(20, 10),
              color: Colors.black,
              activeColor: Colors.blueAccent,
              spacing: const EdgeInsets.symmetric(horizontal: 3),
              activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25),
              )),
        ),
      ),
    );
  }
}