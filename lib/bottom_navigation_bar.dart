import 'package:firstapp/HomeScreen/homescreen.dart';
import 'package:firstapp/categories.dart';
import 'package:firstapp/home.dart';
import 'package:firstapp/login.dart';
import 'package:firstapp/post_list.dart';
import 'package:firstapp/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class BottomNav extends StatefulWidget {
  @override
  _Bottomnav createState() {
    return _Bottomnav();
  }
}

class _Bottomnav extends State<BottomNav> {
  final controler = PersistentTabController(initialIndex: 0);

  List<Widget> buildScreen() {
    return [
      HomeScreen(),
      CategoriesPage(),
      PostsOverviewScreen(),
      const ProfilePage(),
    ];
  }

  List<PersistentBottomNavBarItem> navBarItem() {
    return [
      PersistentBottomNavBarItem(
          icon: const Icon(Icons.home),
          activeColorPrimary: Colors.black,
          inactiveIcon: const Icon(Icons.home, color: Colors.grey)),
      PersistentBottomNavBarItem(
          icon: const Icon(Icons.category),
          activeColorPrimary: Colors.black,
          inactiveIcon: const Icon(Icons.category, color: Colors.grey)),
      PersistentBottomNavBarItem(
          icon: const Icon(Icons.add),
          activeColorPrimary: Colors.black,
          inactiveIcon: const Icon(Icons.add, color: Colors.grey)),
      PersistentBottomNavBarItem(
          icon: const Icon(Icons.person),
          activeColorPrimary: Colors.black,
          inactiveIcon: const Icon(Icons.person, color: Colors.grey)),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const UserAccountsDrawerHeader(
              otherAccountsPictures: [
                CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://img.icons8.com/pastel-glyph/2x/user-male.png'),
                  backgroundColor: Colors.white,
                ),
              ],
              accountName: Text('Neel Modi'),
              accountEmail: Text('neelmodi@gmail.com'),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('images/nature.jpg'),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.login),
              title: const Text('Login'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const MyLogin(),
                  ),
                );
              },
            ),
            const AboutListTile(
              applicationName: 'Apple',
              applicationIcon: Icon(Icons.apple),
              icon: Icon(Icons.safety_check),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: showBottomSheet,
            icon: const Icon(Icons.open_in_new),
          )
        ],
      ),
      body: PersistentTabView(
        context,
        screens: buildScreen(),
        items: navBarItem(),
        controller: controler,
        backgroundColor: Colors.grey.shade100,
        decoration: NavBarDecoration(
            colorBehindNavBar: Colors.red,
            borderRadius: BorderRadius.circular(1)),
        // navBarStyle: NavBarStyle.style10,
      ),
    );
  }

  void showBottomSheet() => showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        // barrierColor: Colors.orange.withOpacity(0.2),
        context: context,
        builder: (context) => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: const Icon(Icons.home),
              title: const Text('Home'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHome(),
                  ),
                );
              },
            ),
          ],
        ),
      );
}
