import 'package:firstapp/bottom_navigation_bar.dart';
import 'package:firstapp/gridview.dart';
import 'package:firstapp/home.dart';
import 'package:firstapp/login.dart';
import 'package:firstapp/radio_button.dart';
import 'package:firstapp/register.dart';
import 'package:flutter/material.dart';

class MyHobbies extends StatefulWidget {
  @override
  _MyHobbies createState() {
    return new _MyHobbies();
  }
}

class _MyHobbies extends State<MyHobbies> {
  bool valuefirst = false;
  bool valuesecond = false;
  bool valuethird = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // drawer: Drawer(
        //   child: ListView(
        //     padding: EdgeInsets.zero,
        //     children: [
        //       const UserAccountsDrawerHeader(
        //         otherAccountsPictures: [
        //           CircleAvatar(
        //             backgroundImage: NetworkImage(
        //                 'https://img.icons8.com/pastel-glyph/2x/user-male.png'),
        //             backgroundColor: Colors.white,
        //           ),
        //         ],
        //         accountName: Text('Neel Modi'),
        //         accountEmail: Text('neelmodi@gmail.com'),
        //         currentAccountPicture: CircleAvatar(
        //           backgroundImage: AssetImage('images/nature.jpg'),
        //         ),
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.login),
        //         title: const Text('Login'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyLogin(),
        //             ),
        //           );
        //         },
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.app_registration),
        //         title: const Text('Register'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyRegister(),
        //             ),
        //           );
        //         },
        //       ),
        //       const AboutListTile(
        //         applicationName: 'Apple',
        //         applicationIcon: Icon(Icons.apple),
        //         icon: Icon(Icons.safety_check),
        //       ),
        //     ],
        //   ),
        // ),
        // appBar: AppBar(
        //   title: const Text('Hobbies 2.O'),
        //   actions: [
        //     IconButton(
        //       onPressed: showBottomSheet,
        //       icon: Icon(Icons.open_in_new),
        //     )
        //   ],
        // ),
        body: Column(
          children: <Widget>[
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.trailing,
              subtitle: const Text('Game of enjoyment'),
              title: const Text('Ludo'),
              value: this.valuefirst,
              onChanged: (value) {
                setState(() {
                  this.valuefirst = value!;
                });
              },
            ),
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.trailing,
              subtitle: const Text('Game of Stamina'),
              title: const Text('Kabaddi'),
              value: this.valuesecond,
              onChanged: (value) {
                setState(() {
                  this.valuesecond = value!;
                });
              },
            ),
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.trailing,
              subtitle: const Text('Brain Game'),
              title: const Text('Chess'),
              value: this.valuethird,
              onChanged: (value) {
                setState(() {
                  this.valuethird = value!;
                });
              },
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Grid(),
                  ),
                );
              },
              child: const Text('Grid view + list view'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyRadio(),
                  ),
                );
              },
              child: const Text('Let us go to radiopage'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BottomNav(),
                  ),
                );
              },
              child: const Text('Bottom Navigation'),
            ),
          ],
        ),
      ),
    );
  }

  void showBottomSheet() => showModalBottomSheet(
        // enableDrag: false,
        // isDismissible: false,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        // barrierColor: Colors.orange.withOpacity(0.2),
        context: context,
        builder: (context) => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHome(),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.radio_button_checked),
              title: Text('Radio'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyRadio(),
                  ),
                );
              },
            ),
          ],
        ),
      );
}
