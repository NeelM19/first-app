import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  final Function(String name, String jobTitle) onProfileUpdated;

  EditProfile(this.onProfileUpdated);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController jobTitleController = TextEditingController();

  /// checkbox

  bool valuefirst = false;
  bool valuesecond = false;
  bool valuethird = false;
  String _gender = '';

  void _toggleGender(String value) {
    setState(() {
      _gender = value;
    });
  }

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: 'Neel Modi');
    jobTitleController = TextEditingController(text: 'Software Engineer');
  }

  @override
  void dispose() {
    nameController.dispose();
    jobTitleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Name',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              TextFormField(
                controller: nameController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter your name';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 10),
              const Text(
                'Job Title',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              TextFormField(
                controller: jobTitleController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter your job title.';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 10),
              /// hobbies and gender
              Row(
                children: [
                  ElevatedButton(
                    onPressed: () => showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        scrollable: true,
                        actions: [
                          Column(
                            children: [
                              CheckboxListTile(
                                controlAffinity: ListTileControlAffinity.trailing,
                                title: const Text('Ludo'),
                                value: this.valuefirst,
                                onChanged: (value) {
                                  setState(() {
                                    this.valuefirst = value!;
                                  });
                                },
                              ),
                              CheckboxListTile(
                                controlAffinity: ListTileControlAffinity.trailing,
                                title: const Text('Kabaddi'),
                                value: this.valuesecond,
                                onChanged: (value) {
                                  setState(() {
                                    this.valuesecond = value!;
                                  });
                                },
                              ),
                              CheckboxListTile(
                                controlAffinity: ListTileControlAffinity.trailing,
                                title: const Text('Chess'),
                                value: this.valuethird,
                                onChanged: (value) {
                                  setState(() {
                                    this.valuethird = value!;
                                  });
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    child: Text('hobbies'),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: () => showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        scrollable: true,
                        actions: [
                          Row(
                            children: [
                              Text('Male'),
                              Radio(
                                value: 'Male',
                                groupValue: _gender,
                                onChanged: (String? value) {
                                  _toggleGender('' + value!);
                                },
                              ),
                              SizedBox(width: 80),
                              Text('Female'),
                              Radio(
                                value: 'Female',
                                groupValue: _gender,
                                onChanged: (String? value) {
                                  _toggleGender('' + value!);
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    child: Text('Gender'),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      String name = nameController.text;
                      String jobTitle = jobTitleController.text;
                      widget.onProfileUpdated(name, jobTitle);
                      Navigator.pop(context);
                    }
                  },
                  child: const Text('Save'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
