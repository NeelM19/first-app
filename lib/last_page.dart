import 'package:firstapp/home.dart';
import 'package:flutter/material.dart';

class LastPage extends StatelessWidget {
  String email, pass;

  LastPage({Key? key, required this.email, required this.pass})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const UserAccountsDrawerHeader(
              accountName: Text('Neel Modi'),
              accountEmail: Text('neelmodi@gmail.com'),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('images/nature.jpg'),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.home),
              title: const Text('Home'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHome(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: const Text('Result'),
      ),
      body: Center(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Email: $email'),
              Text('Pass: $pass'),
              SizedBox(height: 40),
              Column(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MyHome(),
                        ),
                      );
                    },
                    child: const Text('Home Page'),
                  ),
                ],
              ),
              SizedBox(height: 40),
              const Text('           Or go to the drawer in \nthe app bar to go to the home screen'),
            ],
          ),
        ),
      ),
    );
  }
}
