import 'package:firstapp/login.dart';
import 'package:firstapp/register.dart';
import 'package:flutter/material.dart';

class MyRadio extends StatefulWidget {
  @override
  _MyRadio createState() {
    return new _MyRadio();
  }
}

class _MyRadio extends State<MyRadio> {
  String _gender = '';

  void _toggleGender(String value) {
    setState(() {
      _gender = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // drawer: Drawer(
        //   child: ListView(
        //     padding: EdgeInsets.zero,
        //     children: [
        //       const UserAccountsDrawerHeader(
        //         otherAccountsPictures: [
        //           CircleAvatar(
        //             backgroundImage: NetworkImage(
        //                 'https://img.icons8.com/pastel-glyph/2x/user-male.png'),
        //             backgroundColor: Colors.white,
        //           ),
        //         ],
        //         accountName: Text('Neel Modi'),
        //         accountEmail: Text('neelmodi@gmail.com'),
        //         currentAccountPicture: CircleAvatar(
        //           backgroundImage: AssetImage('images/nature.jpg'),
        //         ),
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.login),
        //         title: const Text('Login'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyLogin(),
        //             ),
        //           );
        //         },
        //       ),
        //       ListTile(
        //         leading: const Icon(Icons.app_registration),
        //         title: const Text('Register'),
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => MyRegister(),
        //             ),
        //           );
        //         },
        //       ),
        //       const AboutListTile(
        //         applicationName: 'Apple',
        //         applicationIcon: Icon(Icons.apple),
        //         icon: Icon(Icons.safety_check),
        //       ),
        //     ],
        //   ),
        // ),
        // appBar: AppBar(
        //   title: const Text('Radio Button'),
        // ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Male'),
            Radio(
              value: 'Male',
              groupValue: _gender,
              onChanged: (String? value) {
                _toggleGender('' + value!);
              },
            ),
            Text('Female'),
            Radio(
              value: 'Female',
              groupValue: _gender,
              onChanged: (String? value) {
                _toggleGender('' + value!);
              },
            ),
          ],
        ),
      ),
    );
  }
}
